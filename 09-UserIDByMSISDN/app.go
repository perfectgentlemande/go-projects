package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-redis/redis"
)

// Connecting to Redis
func connect() *redis.Client {

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	resp, err := client.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}

	log.Println(resp)

	return client
}

// Handling HTTP-request to get User by MSISDN
func getUserByMSISDN(w http.ResponseWriter, r *http.Request) {

	val := chi.URLParam(r, "msisdn")

	client := connect()

	// Filling list
	err := client.RPush("MSISDNtoGetUserID", val).Err()
	if err != nil {
		log.Fatal(err)
	}

	w.Write([]byte("MSISDN: " + val + "\n"))
	w.Write([]byte("successfully added to the queue"))
}

func main() {

	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {

		r.Route("/v1", func(r chi.Router) {

			r.Get("/getUserIDByMSISDN/{msisdn}", getUserByMSISDN)
		})

	})

	http.ListenAndServe(":3000", r)
}
