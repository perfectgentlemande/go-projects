package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"

	_ "github.com/lib/pq"
	yaml "gopkg.in/yaml.v2"
)

type Configuration struct {
	Postgres PostgresConfiguration
}

type PostgresConfiguration struct {
	ConnectionString string `yaml:"connectionString"`
}

type ResponsibilityCenterStatistics struct {
	ResponsibilityCenter string
	SpentSum             float64
	Limit                float64
	Remainder            float64
}

func (respCenterStats *ResponsibilityCenterStatistics) setResponsibilityCenterLimit(limit float64) {
	respCenterStats.Limit = limit
}

func (respCenterStats *ResponsibilityCenterStatistics) setResponsibilityCenterRemainder() {
	respCenterStats.Remainder = respCenterStats.Limit - respCenterStats.SpentSum
}

func (respCenterStats *ResponsibilityCenterStatistics) toString() string {
	str := ""
	str += "Name: " + respCenterStats.ResponsibilityCenter + " "
	str += "Spent: " + strconv.FormatFloat(respCenterStats.SpentSum, 'f', -1, 32) + " "
	str += "Remainder: " + strconv.FormatFloat(respCenterStats.Remainder, 'f', -1, 32) + " "
	str += "Remainder: " + strconv.FormatFloat(respCenterStats.Limit, 'f', -1, 32) + " "
	return str
}

func main() {

	// Reading configuration file
	var config Configuration
	fileBytes, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(fileBytes, &config)
	if err != nil {
		log.Fatal(err)
	}

	// Connection to PostgreSQL
	db, err := sql.Open("postgres", config.Postgres.ConnectionString)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Successfully logged in PostgreSQL using: " + config.Postgres.ConnectionString)
	defer db.Close()

	// Selection of rows
	rows, err := db.Query("SELECT \"responsibilityCenter\", SUM(\"spent\") AS sum FROM public.\"TaxiUsers2\" GROUP BY \"responsibilityCenter\" ORDER BY sum")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var responsibilityCenters []ResponsibilityCenterStatistics

	// Select query
	for rows.Next() {
		var stats ResponsibilityCenterStatistics
		err := rows.Scan(&stats.ResponsibilityCenter, &stats.SpentSum)
		if err != nil {
			log.Fatal(err)
		}
		stats.setResponsibilityCenterLimit(1000)
		stats.setResponsibilityCenterRemainder()

		responsibilityCenters = append(responsibilityCenters, stats)
	}

	for _, value := range responsibilityCenters {
		fmt.Println(value.toString())
	}
}
