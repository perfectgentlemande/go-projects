package main

import (
	"fmt"
	"log"

	"github.com/go-redis/redis"
)

// Connecting to Redis
func connect() *redis.Client {

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	resp, err := client.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(resp)

	return client
}

func main() {

	client := connect()

	for {
		// Filling list
		result, err := client.BLPop(0, "test").Result()
		if err != nil {
			panic(err)
		}

		fmt.Println(result[0], result[1])
	}

}
