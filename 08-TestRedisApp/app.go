package main

import (
	"log"

	"github.com/go-redis/redis"
)

// Connecting to Redis
func connect() *redis.Client {

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	resp, err := client.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(resp)

	return client
}

func main() {

	client := connect()

	// Filling list
	err := client.RPush("guys", "Tom", "Ian", "Jon", "John", "Connor").Err()
	if err != nil {
		log.Fatal(err)
	}

	// Getting the first one
	guy := client.LIndex("guys2", 0)
	log.Println(guy.Val())

	// Deleting the first one
	err = client.LPop("guys2").Err()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Deleted: " + guy.Val())
}
