package main

import (
	"bufio"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/tidwall/gjson"
	//"io/ioutil"
	//"log"
	//"net/http"
	//"time"
	// Задействован удобный модуль вот отсюда https://github.com/tidwall/gjson
	//"github.com/tidwall/gjson"
)

// Чтение из файла
func getAddresses(fileName string) []string {

	var addresses []string

	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		addresses = append(addresses, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return addresses
}

func main() {

	addresses := getAddresses("addresses.txt")

	url := "https://geocode-maps.yandex.ru/1.x/?format=json&geocode="

	for _, address := range addresses {

		// Приведение к нужному виду для запроса
		address = strings.Replace(address, " ", "+", -1)

		// Таймаут на 5 секунд
		spaceClient := http.Client{
			Timeout: time.Second * 5,
		}

		// Отправка запроса
		req, err := http.NewRequest(http.MethodGet, url+address, nil)
		if err != nil {
			log.Fatal(err)
		}

		req.Header.Set("User-Agent", "spacecount-tutorial")

		// Получение ответа на запрос
		res, getErr := spaceClient.Do(req)
		if getErr != nil {
			log.Fatal(getErr)
		}

		body, readErr := ioutil.ReadAll(res.Body)
		if readErr != nil {
			log.Fatal(readErr)
		}

		// поскольку body является байтовым массивом, а для использования json-парсера требуется строка
		bodyString := string(body[:])

		// получение данных
		value := gjson.Get(bodyString, "response.GeoObjectCollection.featureMember.0.GeoObject.name")
		println(value.String())
		value = gjson.Get(bodyString, "response.GeoObjectCollection.featureMember.0.GeoObject.Point.pos")
		println(value.String())

	}

}
