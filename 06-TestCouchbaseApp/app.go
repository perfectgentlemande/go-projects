package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/couchbase/gocb"
)

type Airline struct {
	Callsign string `json:"callsign"`
	Country  string `json:"country"`
	IATA     string `json:"iata"`
	ICAO     string `json:"icao"`
	ID       string `json:"id"`
	Name     string `json:"name"`
	Type     string `json:"type"`
}

func main() {

	cluster, err := gocb.Connect("couchbase://127.0.0.1")
	if err != nil {
		fmt.Println("A")
		log.Fatal(err)
	}

	cluster.Authenticate(gocb.PasswordAuthenticator{"Admin", "111111"})

	bucket, err := cluster.OpenBucket("travel-sample", "")
	if err != nil {
		fmt.Println("C")
		log.Fatal(err)
	}

	var airline Airline
	// bucket.Upsert("airline228228", Airline{
	// 	Callsign: "AEROFLOT",
	// 	Country:  "Russian Federation",
	// 	IATA:     "SU",
	// 	ICAO:     "AFL",
	// 	ID:       "228228",
	// 	Name:     "Aeroflot - Russian Airlines",
	// 	Type:     "airline",
	// }, 0)

	bucket.Upsert("airline229229", Airline{
		Callsign: "SIBERIAN AIRLINES",
		Country:  "Russian Federation",
		IATA:     "S7",
		ICAO:     "SBI",
		ID:       "229229",
		Name:     "S7 Airlines",
		Type:     "airline",
	}, 0)

	// bucket.Get("airline228228", &airline)
	// jsonBytes, _ := json.Marshal(airline)
	// fmt.Println(string(jsonBytes))

	bucket.Get("airline229229", &airline)
	jsonBytes, _ := json.Marshal(airline)
	fmt.Println(string(jsonBytes))

	query := gocb.NewN1qlQuery("SELECT * FROM `travel-sample` LIMIT 5")
	rows, err := bucket.ExecuteN1qlQuery(query, nil)
	if err != nil {
		fmt.Println("D")
		log.Fatal(err)
	}

	var row interface{}
	for rows.Next(&row) {
		log.Printf("%+v", row)
	}

}
