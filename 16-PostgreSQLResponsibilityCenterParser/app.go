package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"github.com/lib/pq"
	yaml "gopkg.in/yaml.v2"
)

type Configuration struct {
	Postgres PostgresConfiguration
}

type PostgresConfiguration struct {
	ConnectionString string `yaml:"connectionString"`
}

type TaxiUser struct {
	ID                   string
	Fullname             string
	Phone                string
	Email                string
	IsActive             bool
	RoleName             string
	RoleLimitMoney       float32
	RoleClasses          []string
	Nickname             string
	CostCenter           string
	SpentMoney           float32
	ResponsibilityCenter string
}

func (taxiUser *TaxiUser) setResponsibilityCenter() {

	tempSliceA := strings.Split(taxiUser.Nickname, "|")
	var tempSliceB []string

	for _, value := range tempSliceA {
		value = strings.Replace(value, " ", "", -1)

		if len(value) == 3 {
			tempSliceB = append(tempSliceB, value)
		}
	}

	tempSliceA = tempSliceB

	for _, value := range tempSliceA {
		fmt.Println("item:{" + value + "}")

		if value[2] != 'Z' {
			taxiUser.ResponsibilityCenter = tempSliceA[0]
			break
		}
	}
}

func main() {

	// Reading configuration file
	var config Configuration
	fileBytes, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(fileBytes, &config)
	if err != nil {
		log.Fatal(err)
	}

	// Connection to PostgreSQL
	db, err := sql.Open("postgres", config.Postgres.ConnectionString)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Successfully logged in PostgreSQL using: " + config.Postgres.ConnectionString)
	defer db.Close()

	// Selection of rows
	rows, err := db.Query("select * from \"TaxiUsers\" limit 25")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var taxiUsers []TaxiUser

	// Select query
	for rows.Next() {
		var u TaxiUser
		err := rows.Scan(&u.ID, &u.Fullname, &u.Phone, &u.Email, &u.IsActive, &u.RoleName, &u.RoleLimitMoney, pq.Array(&u.RoleClasses), &u.Nickname, &u.CostCenter, &u.SpentMoney)
		if err != nil {
			log.Fatal(err)
		}
		u.setResponsibilityCenter()

		taxiUsers = append(taxiUsers, u)
	}

	for _, value := range taxiUsers {
		log.Println(value.ResponsibilityCenter)
	}
}
