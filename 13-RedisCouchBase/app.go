package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"

	"github.com/couchbase/gocb"
	"github.com/go-redis/redis"
	yaml "gopkg.in/yaml.v2"
)

// Configuration types
type Configuration struct {
	CouchBase CouchBaseConfiguration
	Redis     RedisConfiguration
}
type CouchBaseConfiguration struct {
	Addr       string `yaml:"addr"`
	Username   string `yaml:"username"`
	Password   string `yaml:"password"`
	BucketName string `yaml:"bucketName"`
}
type RedisConfiguration struct {
	Addr     string `yaml:"addr"`
	Password string `yaml:"password"`
	DB       int    `yaml:"db"`
}

// Data types
type TaxiUser struct {
	ID             string   `json:"_id"`
	Fullname       string   `json:"fullname"`
	Phone          string   `json:"phone"`
	Email          string   `json:"email"`
	IsActive       bool     `json:"active"`
	RoleName       string   `json:"roleName"`
	RoleLimitMoney float32  `json:"roleLimit"`
	RoleClasses    []string `json:"roleClasses"`
	Nickname       string   `json:"nickname"`
	CostCenter     string   `json:"costCenter"`
	SpentMoney     float32  `json:"spent"`
}
type N1qlTaxiUser struct {
	TaxiUser TaxiUser `json:"taxiUser"`
}

// Connecting to Redis
func connect() (*gocb.Bucket, *redis.Client, error) {

	// Reading configuration file
	var config Configuration
	fileBytes, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(fileBytes, &config)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Redis.Addr: " + config.Redis.Addr)
	fmt.Println("Redis.Password: " + config.Redis.Password)
	fmt.Println("Redis.DB: " + strconv.Itoa(config.Redis.DB))

	// Connection to Redis
	client := redis.NewClient(&redis.Options{
		Addr:     config.Redis.Addr,
		Password: config.Redis.Password,
		DB:       config.Redis.DB,
	})

	resp, err := client.Ping().Result()
	log.Println("Successfully logged in Redis. Redis response: " + resp)

	fmt.Println("CouchBase.Addr: " + config.CouchBase.Addr)
	fmt.Println("CouchBase.Username: " + config.CouchBase.Username)
	fmt.Println("CouchBase.Password: " + config.CouchBase.Password)
	fmt.Println("CouchBase.BucketName: " + config.CouchBase.BucketName)

	// Connection to CouchBase
	cluster, err := gocb.Connect(config.CouchBase.Addr)
	if err != nil {
		log.Fatal(err)
	}
	cluster.Authenticate(gocb.PasswordAuthenticator{config.CouchBase.Username, config.CouchBase.Password})
	bucket, err := cluster.OpenBucket(config.CouchBase.BucketName, "")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Successfully logged in CouchBase as: " + config.CouchBase.Username + " to bucket: " + config.CouchBase.BucketName)

	return bucket, client, err
}

type DataBaseController struct {
	couchBaseBucket *gocb.Bucket
	redisClient     *redis.Client
}

// Handling HTTP-request to get User by MSISDN
func (c *DataBaseController) getIDByMSISDN() string {

	bucket := c.couchBaseBucket
	client := c.redisClient

	for {

		// Getting data from Redis
		result, err := client.BLPop(0, "test").Result()
		if err != nil {
			panic(err)
		}

		log.Println("Redis query returns: " + result[1])

		// Getting data from Couchbase
		var taxiUsers []TaxiUser
		query := gocb.NewN1qlQuery("SELECT * FROM `TaxiUsers` AS taxiUser WHERE phone='" + result[1] + "'")
		rows, _ := bucket.ExecuteN1qlQuery(query, nil)
		var row N1qlTaxiUser
		for rows.Next(&row) {
			taxiUsers = append(taxiUsers, row.TaxiUser)
		}

		log.Println("CouchBase query returns: " + result[1])
		for _, item := range taxiUsers {
			log.Printf("Fullname: " + item.Fullname + "; Phone number: " + item.Phone)
		}

	}
}

func main() {

	cbb, red, err := connect()

	if err != nil {
		log.Fatal(err)
	}

	c := DataBaseController{

		couchBaseBucket: cbb,
		redisClient:     red,
	}

	c.getIDByMSISDN()

}
