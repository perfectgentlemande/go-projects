package main

import "fmt"

const d = 5

func main() {

	var (
		hello string
		world string
		num   int
	)

	hello = "Hello"
	world = "World!"
	num = 42

	name := "user"

	var fname, lname, age = "Ivan", "Smith", 228

	fmt.Println(hello, world, num, ","+"...", name, fname, lname, age, d)

	var result = (1+2*3)/2.0 - 2

	fmt.Println(result)

}
