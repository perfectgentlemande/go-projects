package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-chi/chi"
	"github.com/go-redis/redis"
	yaml "gopkg.in/yaml.v2"
)

type RedisConfiguration struct {
	Addr     string
	Password string
	DB       int
}

// Connecting to Redis
func connect() (*redis.Client, error) {

	// Configuration import from YAML file
	var config RedisConfiguration
	fileBytes, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(fileBytes, &config)
	if err != nil {
		log.Fatal(err)
	}

	client := redis.NewClient(&redis.Options{
		Addr:     config.Addr,
		Password: config.Password,
		DB:       config.DB,
	})

	resp, err := client.Ping().Result()

	log.Println(resp)

	return client, err
}

type Controller struct {
	redisClient *redis.Client
}

// Handling HTTP-request to get User by MSISDN
func (c *Controller) getUserByMSISDN(w http.ResponseWriter, r *http.Request) {

	// Status code and data for response
	var statusCode int
	var responseBody = map[string]string{"message": ""}

	val := chi.URLParam(r, "msisdn")

	client := c.redisClient

	// Filling list
	err := client.RPush("MSISDNtoGetUserID", val).Err()
	if err != nil {

		statusCode = http.StatusInternalServerError
		responseBody["message"] = "unsuccessful"
	} else {

		statusCode = http.StatusOK
		responseBody["message"] = "successful"
	}

	respondWithJSON(w, statusCode, responseBody)
}

// Responding with JSON
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	// Response forming
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)

	log.Printf("Response code: %d\n", code)
}

func main() {

	r := chi.NewRouter()

	red, err := connect()

	c := Controller{
		redisClient: red,
	}

	if err != nil {
		log.Fatal(err)
	}

	r.Route("/api", func(r chi.Router) {

		r.Route("/v1", func(r chi.Router) {

			r.Get("/getUserIDByMSISDN/{msisdn}", c.getUserByMSISDN)
		})

	})

	// Running server
	srv := http.Server{
		Addr:    ":3000",
		Handler: r,
	}

	go func() { srv.ListenAndServe() }()
	defer srv.Shutdown(context.Background())

	// Graceful server stop
	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM, syscall.SIGINT)

	sig := <-gracefulStop
	log.Printf("caught sig: %+v", sig)
}
