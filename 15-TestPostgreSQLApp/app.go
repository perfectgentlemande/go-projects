package main

import (
	"database/sql"
	"io/ioutil"
	"log"

	"github.com/lib/pq"
	_ "github.com/lib/pq"
	yaml "gopkg.in/yaml.v2"
)

type Configuration struct {
	Postgres PostgresConfiguration
}

type PostgresConfiguration struct {
	ConnectionString string `yaml:"connectionString"`
}

type TaxiUser struct {
	ID             string
	Fullname       string
	Phone          string
	Email          string
	IsActive       bool
	RoleName       string
	RoleLimitMoney float32
	RoleClasses    []string
	Nickname       string
	CostCenter     string
	SpentMoney     float32
}

func main() {

	// Reading configuration file
	var config Configuration
	fileBytes, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(fileBytes, &config)
	if err != nil {
		log.Fatal(err)
	}

	// Connection to PostgreSQL
	db, err := sql.Open("postgres", config.Postgres.ConnectionString)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Successfully logged in PostgreSQL using: " + config.Postgres.ConnectionString)
	defer db.Close()

	// Selection of rows
	rows, err := db.Query("select * from \"TaxiUsers\" limit 25")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var taxiUsers []TaxiUser

	for rows.Next() {
		var u TaxiUser
		err := rows.Scan(&u.ID, &u.Fullname, &u.Phone, &u.Email, &u.IsActive, &u.RoleName, &u.RoleLimitMoney, pq.Array(&u.RoleClasses), &u.Nickname, &u.CostCenter, &u.SpentMoney)
		if err != nil {
			log.Fatal(err)
		}
		taxiUsers = append(taxiUsers, u)
	}

	for _, value := range taxiUsers {
		log.Println(value)
	}

}
