package main

import (
	"database/sql"
	"io/ioutil"
	"log"
	"strings"

	"github.com/couchbase/gocb"
	_ "github.com/lib/pq"
	yaml "gopkg.in/yaml.v2"
)

type Configuration struct {
	Couchbase CouchbaseConfiguration
	Postgres  PostgresConfiguration
}

type CouchbaseConfiguration struct {
	Addr       string `yaml:"addr"`
	Username   string `yaml:"username"`
	Password   string `yaml:"password"`
	BucketName string `yaml:"bucketName"`
}
type PostgresConfiguration struct {
	ConnectionString string `yaml:"connectionString"`
}

type TaxiUser struct {
	ID                   string   `json:"_id"`
	Fullname             string   `json:"fullname"`
	Phone                string   `json:"phone"`
	Email                string   `json:"email"`
	IsActive             bool     `json:"active"`
	RoleName             string   `json:"roleName"`
	RoleLimitMoney       float32  `json:"roleLimit"`
	RoleClasses          []string `json:"roleClasses"`
	Nickname             string   `json:"nickname"`
	CostCenter           string   `json:"costCenter"`
	SpentMoney           float32  `json:"spent"`
	ResponsibilityCenter string
}
type N1qlTaxiUser struct {
	TaxiUser TaxiUser `json:"taxiUser"`
}

func (taxiUser *TaxiUser) setResponsibilityCenter() {

	tempSliceA := strings.Split(taxiUser.Nickname, "|")
	for _, value := range tempSliceA {
		value = strings.Replace(value, " ", "", -1)

		if len(value) == 3 && !strings.ContainsAny(value, "0123456789") {
			taxiUser.ResponsibilityCenter = value
			break
		}
	}
}

func toArrayString(stringSlice []string) string {

	str := "{"

	for index, value := range stringSlice {
		str += ("\"" + value + "\"")
		if index+1 != len(stringSlice) {
			str += ","
		}
	}

	str += "}"

	return str
}

func main() {

	// Reading configuration file
	var config Configuration
	fileBytes, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(fileBytes, &config)
	if err != nil {
		log.Fatal(err)
	}

	// Connection to Couchbase
	cluster, err := gocb.Connect(config.Couchbase.Addr)
	if err != nil {
		log.Fatal(err)
	}
	cluster.Authenticate(gocb.PasswordAuthenticator{config.Couchbase.Username, config.Couchbase.Password})
	bucket, err := cluster.OpenBucket(config.Couchbase.BucketName, "")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Successfully logged in Couchbase as: " + config.Couchbase.Username + " to bucket: " + config.Couchbase.BucketName)

	// Connection to PostgreSQL
	db, err := sql.Open("postgres", config.Postgres.ConnectionString)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Successfully logged in PostgreSQL using: " + config.Postgres.ConnectionString)
	defer db.Close()

	// Getting data from Couchbase
	var taxiUsers []TaxiUser
	query := gocb.NewN1qlQuery("SELECT * FROM `TaxiUsers` AS taxiUser")
	rows, _ := bucket.ExecuteN1qlQuery(query, nil)
	var row N1qlTaxiUser
	for rows.Next(&row) {
		row.TaxiUser.setResponsibilityCenter()
		taxiUsers = append(taxiUsers, row.TaxiUser)
	}
	for _, value := range taxiUsers {
		log.Println(value)

		result, err := db.Exec(`INSERT INTO public."TaxiUsers2"(
			"_id", "fullname", "phone", "email", "active", "roleName", "roleLimit", "roleClasses", "nickname", "costCenter", "spent", "responsibilityCenter")
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);`, value.ID, value.Fullname, value.Phone, value.Email, value.IsActive, value.RoleName, value.RoleLimitMoney, toArrayString(value.RoleClasses), value.Nickname, value.CostCenter, value.SpentMoney, value.ResponsibilityCenter)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(result.RowsAffected()) // количество добавленных строк
	}
}
