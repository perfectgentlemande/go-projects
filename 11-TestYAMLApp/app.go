package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// Structure for redis configuration
type RedisConfiguration struct {
	Addr string
	// "localhost:6379",
	Password string
	// "", // no password set
	DB int
	//    0,  // use default DB
}

func main() {

	// Тестовая конфигурация
	var config RedisConfiguration
	config.Addr = "1.1.1.1"
	config.Password = "abcds"
	config.DB = 9

	// Получение байт-кода
	data, err := yaml.Marshal(&config)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	// запись в файл
	file, err := os.Create("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	file.Write(data)

	fmt.Println("Done.")

	var newConfig RedisConfiguration

	// Чтение из файла
	fileBytes, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(fileBytes, &newConfig)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(newConfig)

}
