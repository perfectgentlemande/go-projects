// Тестовое веб-приложение с подключением к MongoDB и получением оттуда выборки из тестовой коллекции
package main

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Band struct {
	ID                  bson.ObjectId `bson:"_id"`
	Name                string        `bson:"name"`
	YearOfEstablishment string        `bson:"established"`
	Styles              []string      `bson:"styles"`
}

func main() {

	// После перехода по ссылке выводится выборка из тестовой базы данных MongoDB
	http.HandleFunc("/bands", func(w http.ResponseWriter, r *http.Request) {

		fmt.Fprintln(w, "Bands")

		// Открытие соединения
		session, err := mgo.Dial("mongodb://127.0.0.1")
		if err != nil {
			panic(err)
		}
		defer session.Close()

		// Получение коллекции
		bandCollection := session.DB("musictest").C("bands")
		// критерий выборки
		query := bson.M{}
		// объект для сохранения результата
		bands := []Band{}
		bandCollection.Find(query).All(&bands)

		// Вывод групп в браузер и в терминл
		for _, b := range bands {
			fmt.Fprintln(w, b.Name, b.YearOfEstablishment, b.Styles)
			fmt.Println(b.Name, b.YearOfEstablishment, b.Styles)
		}
	})

	fmt.Println("Server is listening...")
	http.ListenAndServe("localhost:8181", nil)
}
